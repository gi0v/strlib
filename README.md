# Strlib

## _A simple libary for string support in C_

## Getting started

```c
#define STRLIB_IMPLEMENTATION
#include "./strlib.h"

/* Have fun! */
```

## Information

This is an [stb-style single-header-only library](https://www.github.com/nothings/stb.git). Which is not made for performace, neither for size and also not for memory optimization, it is rather made to make C programming with strings (char arrays) a bit more pleasant.

## Note

I've personally tested all the procedures that come with this library in C and also tested it with [valgrind](https://valgrind.org/) for memory leaks & errors. However I cannot guarantee that this will work on your machine nor that it will suit your needs.
